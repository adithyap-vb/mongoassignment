const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

const getUser = require("./usersApi/getUser.js");
const addUser = require("./usersApi/addUser.js");
const deleteUser = require("./usersApi/deleteUser.js");
const updateUser = require("./usersApi/updateUser.js");
getUser();
addUser("newUser");
updateUser("newUser", "newUser2");
deleteUser("newUser2");
