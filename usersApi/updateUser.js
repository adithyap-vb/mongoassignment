const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

function updateUser(userName, newName) {
  const client = new MongoClient(url);
  const dbName = "users";
  client.connect();
  const db = client.db(dbName);
  const collection = db.collection("usersCollection");
  collection
    .findOneAndUpdate({ name: userName }, { $set: { name: newName } })
    .then((err, res) => {
      if (err) {
        console.log(err);
      } else {
        console.log(res);
      }
    });
  return "User updated";
}

module.exports = updateUser;
