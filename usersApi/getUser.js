const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

function getUser() {
  const client = new MongoClient(url);
  const dbName = "users";
  client.connect();
  const db = client.db(dbName);
  const collection = db.collection("usersCollection");
  const cursor = collection.find({});
  cursor.forEach((item) => {
    console.log(item);
  });
  return "Get Users";
}

module.exports = getUser;
