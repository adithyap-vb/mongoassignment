const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

function deleteUser(userName) {
  const client = new MongoClient(url);
  const dbName = "users";
  console.log("client is " + client);
  client.connect();
  console.log("Connected successfully to server");
  const db = client.db(dbName);
  const collection = db.collection("usersCollection");
  collection.deleteMany({ name: userName }).then((err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log(res);
    }
  });

  return "User deleted";
}

module.exports = deleteUser;
