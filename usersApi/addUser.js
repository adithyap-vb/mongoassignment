const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

function addUser(userName) {
  const client = new MongoClient(url);
  const dbName = "users";
  console.log("client is " + client);
  client.connect();
  console.log("Connected successfully to server");
  const db = client.db(dbName);
  const collection = db.collection("usersCollection");
  collection.insertOne({ name: userName }).then((err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log(res);
    }
  });
  client.close();
  return "User added";
}

module.exports = addUser;
